//
//  CustomTableViewCell.h
//  Hi Pages
//
//  Created by Rakesh Kumar on 25/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UITextField *valuefield;

@property (strong, nonatomic) IBOutlet UIButton *droptapbutton;


@property (strong, nonatomic) IBOutlet UILabel *totalTitle;

@property (strong, nonatomic) IBOutlet UILabel *pricelabel;
@property (strong, nonatomic) IBOutlet UILabel *unitlabel;

@end
