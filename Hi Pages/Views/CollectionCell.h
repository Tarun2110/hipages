//
//  CollectionCell.h
//  Foodgasm
//
//  Created by Rakesh Kumar on 24/02/16.
//  Copyright © 2016 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *ServiceName;
@property (weak, nonatomic) IBOutlet UIImageView *ServiceImage;

@end
