//
//  BookingViewController.m
//  Hi Pages
//
//  Created by Rakesh Kumar on 08/11/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "BookingViewController.h"

@interface BookingViewController ()
@property (assign) NSInteger expandedHeaderNumber;
@property (assign) UITableViewHeaderFooterView *expandedHeader;

@end

@implementation BookingViewController
{
    NSMutableArray * tenderingOrdersArray;
    NSMutableArray * serviceOrdersArray;
    NSMutableArray * subserviceArray;

    int i;
}


#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tabBarController.tabBar setHidden:YES];

    tenderingOrdersArray = [NSMutableArray new];
    serviceOrdersArray = [NSMutableArray new];
    
    [segmentController addTarget:self action:@selector(segmentedControlValueDidChange:) forControlEvents: UIControlEventValueChanged];
    segmentController.selectedSegmentIndex = 0;
    i = 0;
    
    _bookingTableView.delegate = self;
    _bookingTableView.dataSource = self;
    
    self.bookingTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.bookingTableView.rowHeight = UITableViewAutomaticDimension;
    self.bookingTableView.estimatedRowHeight = 100;
    self.expandedHeaderNumber = -1;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    NSString *Userid = [UserDefaults valueForKey:@"UserId"];
    [self ShowProgress];
    [self.APIService API_BookingHistory:self withSelector:@selector(outputForBookingHistory:) :self.view :Userid];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}

- (IBAction)TappedAction_BackButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) outputForBookingHistory:(NSDictionary *)responseDict
{
    [self HideProgress];
    int  successString = [[responseDict valueForKey:@"status"] intValue];
    if (successString == 1)
    {
        tenderingOrdersArray = [responseDict valueForKey:@"tendering_orders"];
        serviceOrdersArray = [responseDict valueForKey:@"service_orders"];
       
        if (tenderingOrdersArray.count >0)
        {
            [_bookingTableView reloadData];
            [_bookingTableView setHidden:NO];
        }
        else
        {
            [_bookingTableView setHidden:YES];
            [self.GlobelFucntion alert:@"Alert!" :@"No bookings yet" :self];
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (i == 0)
    {
        return tenderingOrdersArray.count;
    }
    else
    {
        return serviceOrdersArray.count;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.expandedHeaderNumber == section)
    {
        return tenderingOrdersArray.count;
    }
    else
    {
        return 0;
    }
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *bgview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 120)];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 115)];
    view.layer.masksToBounds = NO;
    view.layer.borderColor = [UIColor lightGrayColor].CGColor;
    view.layer.borderWidth = 1.0f;
    view.layer.shadowOpacity = 0.55f;
    view.layer.shadowRadius = 1.0f;
    view.layer.shadowOffset = CGSizeZero;
   
    NSString *status;
    UILabel * Titlelabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 250, 20)];
    [Titlelabel setFont:[UIFont fontWithName:@"Gill Sans" size:14]];
    Titlelabel.textColor = [UIColor colorWithRed:66/255.0 green:142/255.0 blue:241/255.0 alpha:1.0];
    
    UILabel * Statuslabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 138, 5, 120, 20)];
    Statuslabel.textAlignment = NSTextAlignmentRight;
    [Statuslabel setFont:[UIFont fontWithName:@"Gill Sans" size:15]];

    UILabel * linelabel = [[UILabel alloc] initWithFrame:CGRectMake(10, view.frame.origin.y + 30, tableView.frame.size.width-20, 1)];
    linelabel.backgroundColor = [UIColor lightGrayColor];
    
    UILabel * Orderlabel = [[UILabel alloc] initWithFrame:CGRectMake(10, linelabel.frame.origin.y + 5, 200, 20)];
    [Orderlabel setFont:[UIFont fontWithName:@"Gill Sans" size:12]];
    [Orderlabel setText:@"Order Number"];
    Orderlabel.textColor = [UIColor darkGrayColor];
    
    UILabel * OrderNoLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, Orderlabel.frame.origin.y + 15, 200, 20)];
    [OrderNoLabel setFont:[UIFont fontWithName:@"Gill Sans" size:10]];
    OrderNoLabel.textColor = [UIColor darkGrayColor];
    
    
    UILabel * Shippinglabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 100, linelabel.frame.origin.y + 5, 200, 20)];
    [Shippinglabel setFont:[UIFont fontWithName:@"Gill Sans" size:12]];
    [Shippinglabel setText:@"Shipping Address"];
    Shippinglabel.textColor = [UIColor darkGrayColor];

    UILabel * Addresslabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 200, Shippinglabel.frame.origin.y + 15, 180, 40)];
    Addresslabel.numberOfLines = 4;
    Addresslabel.lineBreakMode = true;
    Addresslabel.textAlignment = NSTextAlignmentRight;
    [Addresslabel setFont:[UIFont fontWithName:@"Gill Sans" size:10]];
    Addresslabel.textColor = [UIColor darkGrayColor];
    
    UILabel * Baselinelabel = [[UILabel alloc] initWithFrame:CGRectMake(10, Addresslabel.frame.origin.y + 40, tableView.frame.size.width-20, 1)];
    Baselinelabel.backgroundColor = [UIColor lightGrayColor];

    
    UILabel * OrderDatelabel = [[UILabel alloc] initWithFrame:CGRectMake(10, Baselinelabel.frame.origin.y + 3, tableView.frame.size.width-20, 20)];
    [OrderDatelabel setFont:[UIFont fontWithName:@"Gill Sans" size:12]];
    OrderDatelabel.textColor = [UIColor darkGrayColor];
    
    if (i == 0)
    {
        status = [[tenderingOrdersArray valueForKey:@"status"] objectAtIndex:section];
        
        if ([status isEqualToString:@"1"])
        {
            [Statuslabel setText:@"Complete"];
             Statuslabel.textColor = [UIColor colorWithRed:18/255.0 green:127/255.0 blue:0/255.0 alpha:1.0];
        }
        else
        {
            [Statuslabel setText:@"Pending"];
            Statuslabel.textColor = [UIColor colorWithRed:247/255.0 green:208/255.0 blue:26/255.0 alpha:1.0];
        }
        
        [Titlelabel setText:[[tenderingOrdersArray valueForKey:@"service"] objectAtIndex:section]];
        [OrderNoLabel setText:[[tenderingOrdersArray valueForKey:@"orderno"] objectAtIndex:section]];
        [Addresslabel setText:[[tenderingOrdersArray valueForKey:@"address"] objectAtIndex:section]];
        [OrderDatelabel setText:[NSString stringWithFormat:@"Order Placed : %@ , %@",[[tenderingOrdersArray valueForKey:@"date"] objectAtIndex:section],[[tenderingOrdersArray valueForKey:@"time"] objectAtIndex:section]]];
         }
    else
    {
        status = [[serviceOrdersArray valueForKey:@"status"] objectAtIndex:section];
        
        if ([status isEqualToString:@"0"])
        {
            [Statuslabel setText:@"Complete"];
            Statuslabel.textColor = [UIColor colorWithRed:66/255.0 green:142/255.0 blue:241/255.0 alpha:1.0];
        }
        else
        {
            [Statuslabel setText:@"Pending"];
            Statuslabel.textColor = [UIColor redColor];
        }
        
        [OrderNoLabel setText:[[serviceOrdersArray valueForKey:@"orderno"] objectAtIndex:section]];
        
        [Addresslabel setText:[[serviceOrdersArray valueForKey:@"address"] objectAtIndex:section]];
        
        [OrderDatelabel setText:[NSString stringWithFormat:@"Order Placed : %@ , %@",[[serviceOrdersArray valueForKey:@"date"] objectAtIndex:section],[[serviceOrdersArray valueForKey:@"time"] objectAtIndex:section]]];
   
        view.tag = section;
        
        UITapGestureRecognizer *headerTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderWasTouched:)];
      
        [view addGestureRecognizer:headerTapGesture];
    }

   
    bgview.backgroundColor = [UIColor whiteColor];
    view.backgroundColor = [UIColor whiteColor];

    [view addSubview:Statuslabel];
    [view addSubview:Orderlabel];
    [view addSubview:OrderNoLabel];
    [view addSubview:Titlelabel];

    [view addSubview:Shippinglabel];
    [view addSubview:Addresslabel];
    [view addSubview:OrderDatelabel];

    [bgview addSubview:view];
    [view addSubview:linelabel];
    [view addSubview:Baselinelabel];

    return bgview;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"tableCell" forIndexPath:indexPath];
    cell.textLabel.textColor = [UIColor blackColor];
    //cell.textLabel.text = [[subCategoryArray valueForKey:@"description"]objectAtIndex:indexPath.section] ;
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
{
    return 120;
}


-(void)segmentedControlValueDidChange:(UISegmentedControl *)segment
{
    switch (segment.selectedSegmentIndex)
    {
        case 0:
        {
            i=0;
            if (tenderingOrdersArray.count > 0)
            {
                [_bookingTableView reloadData];
                [_bookingTableView setHidden:NO];
            }
            else
            {
                [_bookingTableView setHidden:YES];
                [self.GlobelFucntion alert:@"Alert!" :@"No bookings yet" :self];
            }
            break;
        }
        case 1:
        {
            i = 1;
            
            if (serviceOrdersArray.count > 0)
            {
                [_bookingTableView reloadData];
                [_bookingTableView setHidden:NO];
            }
            else
            {
                [_bookingTableView setHidden:YES];
                [self.GlobelFucntion alert:@"Alert!" :@"No bookings yet" :self];
            }
            break;
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)updateTableViewRowDisplay:(NSArray *)arrayOfIndexPaths
{
    [self.bookingTableView beginUpdates];
    [self.bookingTableView deleteRowsAtIndexPaths:arrayOfIndexPaths withRowAnimation: UITableViewRowAnimationFade];
    [self.bookingTableView endUpdates];
}


- (void)sectionHeaderWasTouched:(UITapGestureRecognizer *)sender
{
    UITableViewHeaderFooterView *headerView = (UITableViewHeaderFooterView *)sender.view;
    NSInteger section = headerView.tag;
    UIImageView *eImageView = (UIImageView *)[headerView viewWithTag:10 + section];
    self.expandedHeader = headerView;
    
    if (self.expandedHeaderNumber == -1)
    {
        self.expandedHeaderNumber = section;
        [self tableViewExpandSection:section withImage: eImageView];
    } else {
        if (self.expandedHeaderNumber == section) {
            [self tableViewCollapeSection:section withImage: eImageView];
            self.expandedHeader = nil;
        } else {
            UIImageView *cImageView  = (UIImageView *)[self.view viewWithTag:10 + self.expandedHeaderNumber];
            [self tableViewCollapeSection:self.expandedHeaderNumber withImage: cImageView];
            [self tableViewExpandSection:section withImage: eImageView];
        }
    }
}

- (void)tableViewCollapeSection:(NSInteger)section withImage:(UIImageView *)imageView
{
    NSArray *sectionData = tenderingOrdersArray;
    
    self.expandedHeaderNumber = -1;
    if (sectionData.count == 0) {
        return;
    } else {
        [UIView animateWithDuration:0.4 animations:^{
            imageView.transform = CGAffineTransformMakeRotation((0.0 * M_PI) / 180.0);
        }];
        NSMutableArray *arrayOfIndexPaths = [NSMutableArray array];
        for (int ik=0; ik< sectionData.count; ik++) {
            NSIndexPath *index = [NSIndexPath indexPathForRow:ik inSection:section];
            [arrayOfIndexPaths addObject:index];
        }
        [self.bookingTableView beginUpdates];
        [self.bookingTableView deleteRowsAtIndexPaths:arrayOfIndexPaths withRowAnimation: UITableViewRowAnimationFade];
        [self.bookingTableView endUpdates];
    }
}

- (void)tableViewExpandSection:(NSInteger)section withImage:(UIImageView *)imageView {
    
    NSArray *sectionData = tenderingOrdersArray  ;
    
    if (sectionData.count == 0)
    {
        self.expandedHeaderNumber = -1;
        return;
    } else
    {
        [UIView animateWithDuration:0.4 animations:^{
            imageView.transform = CGAffineTransformMakeRotation((180.0 * M_PI) / 180.0);
        }];
        NSMutableArray *arrayOfIndexPaths = [NSMutableArray array];
        for (int ik=0; ik< sectionData.count; ik++)
        {
            NSIndexPath *index = [NSIndexPath indexPathForRow:ik inSection:section];
            [arrayOfIndexPaths addObject:index];
        }
        self.expandedHeaderNumber = section;
        [self.bookingTableView beginUpdates];
        [self.bookingTableView insertRowsAtIndexPaths:arrayOfIndexPaths withRowAnimation: UITableViewRowAnimationFade];
        [self.bookingTableView endUpdates];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
