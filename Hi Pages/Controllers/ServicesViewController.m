//
//  ServicesViewController.m
//  Hi Pages
//
//  Created by Rakesh Kumar on 05/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "ServicesViewController.h"
#import "SubCategories.h"
#import "DetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CollectionCell.h"
#import "CategoryObject.h"
#import <QuartzCore/QuartzCore.h>

@interface ServicesViewController ()
@end
@implementation ServicesViewController
{
    NSMutableArray *TotalServicesArray;
}

#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self ShowProgress];
    TotalServicesArray = [NSMutableArray new];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self.APIService API_GetAllServices:self withSelector:@selector(OutputForAllServices:) :self.view];
    [self.tabBarController.tabBar setHidden:NO];
}


- (void) OutputForAllServices:(NSDictionary *)responseDict
{
    [self HideProgress];
    int  successString = [[responseDict valueForKey:@"status"] intValue];
    if (successString == 1)
    {
        NSMutableArray *ServicesArray = [responseDict valueForKey:@"services"];

        for (int i = 0 ; i != ServicesArray.count ; i++)
        {
            CategoryObject * serviceCat = [CategoryObject new];
            
            serviceCat.categoryID =[[ServicesArray valueForKey:@"id"] objectAtIndex:i];
            serviceCat.categoryName = [[ServicesArray valueForKey:@"category_name"] objectAtIndex:i];
            serviceCat.categoryChild = [[ServicesArray valueForKey:@"child_category"] objectAtIndex:i];
            serviceCat.categoryImage = [[ServicesArray valueForKey:@"category_image"] objectAtIndex:i];
            
            [TotalServicesArray addObject:serviceCat];
        }
        _CollectioView.delegate = self;
        _CollectioView.dataSource = self;
    }
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}


# pragma mark  CollectionView Delegates
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection: (NSInteger)section
{
    return TotalServicesArray.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionCell *Cell = (CollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    Cell.contentView.frame = Cell.bounds;
    
    Cell.ServiceImage.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin  | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleTopMargin |UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    
    Cell.layer.masksToBounds = NO;
    Cell.layer.borderColor = [UIColor whiteColor].CGColor;
    Cell.layer.borderWidth = 2.0f;
    Cell.layer.contentsScale = [UIScreen mainScreen].scale;
    Cell.layer.shadowOpacity = 0.55f;
    Cell.layer.shadowRadius = 2.0f;
    Cell.layer.shadowOffset = CGSizeZero;
    Cell.layer.shadowPath = [UIBezierPath bezierPathWithRect:Cell.bounds].CGPath;
    Cell.layer.shouldRasterize = YES;
    
    CategoryObject * CategoryObj = [TotalServicesArray objectAtIndex:indexPath.row];

    Cell.ServiceName.text = CategoryObj.categoryName ;
 
    [Cell.ServiceImage sd_setImageWithURL:[NSURL URLWithString:CategoryObj.categoryImage]
                            placeholderImage:[UIImage imageNamed:@"placeholder.png"]];

    return Cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100, 100);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
   // if(IS_IPHONE_5)
    {
        return UIEdgeInsetsMake(2, 2.5, 2, 2.5); // top, left, bottom, right
    }
//    else if (IS_IPHONE_6)
//    {
//        return UIEdgeInsetsMake(10, 18, 10, 18); // top, left, bottom, right
//    }
//    else if (IS_IPHONE_6_PLUS)
//    {
//        return UIEdgeInsetsMake(25, 25, 25, 25); // top, left, bottom, right
//    }
//    return UIEdgeInsetsMake(25, 20, 25, 20); // top, left, bottom, right
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryObject * CategoryObj = [TotalServicesArray objectAtIndex:indexPath.row];

    if ([CategoryObj.categoryChild  isEqual: @"true"])
    {
        SubCategories *controller = VCWithIdentifier(@"SubCategories");
        controller.ServiceId = CategoryObj.categoryID;
        controller.HeaderName = CategoryObj.categoryName;
        [self.navigationController pushViewController:controller animated:YES];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults ]setBool:NO forKey:@"subclass"];
        DetailViewController *controller = VCWithIdentifier(@"DetailViewController");
        controller.HeaderName = CategoryObj.categoryName;
        controller.DataObj = CategoryObj;
        [self.navigationController pushViewController:controller animated:YES];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



@end
