//
//  EditProfileViewController.h
//  Hi Pages
//
//  Created by Rakesh Kumar on 06/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface EditProfileViewController : BaseViewController
@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (strong, nonatomic) IBOutlet UIButton *changePasswordButton;
@property (strong, nonatomic) IBOutlet UIImageView *EditProfilePic;
@property (strong, nonatomic) IBOutlet UIButton *SaveButton;

@property (strong, nonatomic) IBOutlet UITextField *textfield_firstname;
@property (strong, nonatomic) IBOutlet UITextField *textfield_phoneNumber;
@property (strong, nonatomic) IBOutlet UITextField *textfield_email;
@property (strong, nonatomic) IBOutlet UITextField *textfield_lastname;

@property (strong, nonatomic) IBOutlet NSString *firstname;
@property (strong, nonatomic) IBOutlet NSString *lastname;
@property (strong, nonatomic) IBOutlet NSString *phoneNumber;
@property (strong, nonatomic) IBOutlet NSString *email;

@property (strong, nonatomic) IBOutlet NSString *UserId;


@end
