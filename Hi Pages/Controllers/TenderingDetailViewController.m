//
//  TenderingDetailViewController.m
//  Hi Pages
//
//  Created by Rakesh Kumar on 01/11/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "TenderingDetailViewController.h"
#import "CustomTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MakeBookingViewController.h"


@interface TenderingDetailViewController ()
{
    NSMutableArray *DescArray;
    
}
@property (assign) NSInteger expandedSectionHeaderNumber;
@property (assign) UITableViewHeaderFooterView *expandedSectionHeader;

@end

@implementation TenderingDetailViewController

#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [self.tabBarController.tabBar setHidden:YES];
    
    _TitleLabel.text = _tenderObj.TenderName;
    _descriptionLabel.text = _tenderObj.TenderPagetitle;
    
    subCategoryArray = [NSMutableArray new];
    subCategoryArray = _tenderObj.TenderDescription;
    
    [_ItemImage sd_setImageWithURL:[NSURL URLWithString:_tenderObj.TenderImage]
                         placeholderImage:[UIImage imageNamed:@"placeholder.png"]];

    DescArray = [NSMutableArray new];
    [DescArray addObject:@""];
    
    _bookingButton.layer.cornerRadius=_bookingButton.frame.size.height/2;
    _bookingButton.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _bookingButton.layer.borderWidth=1.0f;
    _bookingButton.clipsToBounds = YES;
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView reloadData];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100;
    self.expandedSectionHeaderNumber = -1;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}

#pragma mark - Tapped Actions

- (IBAction)TappedAction_backButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)TappedAction_MakeBooking:(id)sender
{
    MakeBookingViewController * controller = VCWithIdentifier(@"MakeBookingViewController");
    controller.TenderName = _tenderObj.TenderName;
    controller.TenderId = _tenderObj.TenderID;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (subCategoryArray.count > 0)
    {
        return subCategoryArray.count;
    }
    return 0;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.expandedSectionHeaderNumber == section)
    {
        return DescArray.count;
    }
    else
    {
        return 0;
    }
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, tableView.frame.size.width-40, 49)];
    [label setFont:[UIFont fontWithName:@"Gill Sans" size:15]];
    NSString *string =[[subCategoryArray valueForKey:@"title"] objectAtIndex:section];
    [label setText:string];
    
    label.textColor = [UIColor colorWithRed:66/255.0 green:142/255.0 blue:241/255.0 alpha:1.0];
    UILabel *linelabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 49, tableView.frame.size.width-20, 1)];
    linelabel.backgroundColor = [UIColor lightGrayColor];
   
    view.tag = section;
    view.backgroundColor = [UIColor whiteColor];
    UITapGestureRecognizer *headerTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderWasTouched:)];
    [view addGestureRecognizer:headerTapGesture];
    [view addSubview:label];
    [view addSubview:linelabel];
    
    return view;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
{
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"tableCell" forIndexPath:indexPath];
    cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.text = [[subCategoryArray valueForKey:@"description"]objectAtIndex:indexPath.section] ;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)updateTableViewRowDisplay:(NSArray *)arrayOfIndexPaths
{
    [self.tableView beginUpdates];
    [self.tableView deleteRowsAtIndexPaths:arrayOfIndexPaths withRowAnimation: UITableViewRowAnimationFade];
    [self.tableView endUpdates];
}

#pragma mark - Expand / Collapse Methods

- (void)sectionHeaderWasTouched:(UITapGestureRecognizer *)sender
{
    UITableViewHeaderFooterView *headerView = (UITableViewHeaderFooterView *)sender.view;
    NSInteger section = headerView.tag;
    UIImageView *eImageView = (UIImageView *)[headerView viewWithTag:10 + section];
    self.expandedSectionHeader = headerView;
    
    if (self.expandedSectionHeaderNumber == -1)
    {
        self.expandedSectionHeaderNumber = section;
        [self tableViewExpandSection:section withImage: eImageView];
    } else {
        if (self.expandedSectionHeaderNumber == section) {
            [self tableViewCollapeSection:section withImage: eImageView];
            self.expandedSectionHeader = nil;
        } else {
            UIImageView *cImageView  = (UIImageView *)[self.view viewWithTag:10 + self.expandedSectionHeaderNumber];
            [self tableViewCollapeSection:self.expandedSectionHeaderNumber withImage: cImageView];
            [self tableViewExpandSection:section withImage: eImageView];
        }
    }
}

- (void)tableViewCollapeSection:(NSInteger)section withImage:(UIImageView *)imageView
{
    NSArray *sectionData = DescArray;
    
    self.expandedSectionHeaderNumber = -1;
    if (sectionData.count == 0) {
        return;
    } else {
        [UIView animateWithDuration:0.4 animations:^{
            imageView.transform = CGAffineTransformMakeRotation((0.0 * M_PI) / 180.0);
        }];
        NSMutableArray *arrayOfIndexPaths = [NSMutableArray array];
        for (int i=0; i< sectionData.count; i++) {
            NSIndexPath *index = [NSIndexPath indexPathForRow:i inSection:section];
            [arrayOfIndexPaths addObject:index];
        }
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:arrayOfIndexPaths withRowAnimation: UITableViewRowAnimationFade];
        [self.tableView endUpdates];
    }
}

- (void)tableViewExpandSection:(NSInteger)section withImage:(UIImageView *)imageView {
    
    NSArray *sectionData = DescArray  ;
    
    if (sectionData.count == 0)
    {
        self.expandedSectionHeaderNumber = -1;
        return;
    } else
    {
        [UIView animateWithDuration:0.4 animations:^{
            imageView.transform = CGAffineTransformMakeRotation((180.0 * M_PI) / 180.0);
        }];
        NSMutableArray *arrayOfIndexPaths = [NSMutableArray array];
        for (int i=0; i< sectionData.count; i++)
        {
            NSIndexPath *index = [NSIndexPath indexPathForRow:i inSection:section];
            [arrayOfIndexPaths addObject:index];
        }
        self.expandedSectionHeaderNumber = section;
        [self.tableView beginUpdates];
        [self.tableView insertRowsAtIndexPaths:arrayOfIndexPaths withRowAnimation: UITableViewRowAnimationFade];
        [self.tableView endUpdates];
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
