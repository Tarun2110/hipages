//
//  ViewController.h
//  Hi Pages
//
//  Created by Rakesh Kumar on 19/09/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface RootViewController : BaseViewController<UIScrollViewAccessibilityDelegate,UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UIView *BannerView;
@property (strong, nonatomic) IBOutlet UITextField *numberTextField;
@property (strong, nonatomic) IBOutlet UIButton *proceedButton;

@end

