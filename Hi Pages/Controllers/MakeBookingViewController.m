//
//  MakeBookingViewController.m
//  Hi Pages
//
//  Created by Rakesh Kumar on 02/11/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "MakeBookingViewController.h"
#import "ConfirmBookingController.h"


@interface MakeBookingViewController ()

@end

@implementation MakeBookingViewController
{
    NSArray *timeArray;
    
}
#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _ServiceNameTextfield.text = _TenderName;
 
    _ServiceTimeTextfield.delegate = self;
     timeArray = [NSArray arrayWithObjects: @"ASAP", @"Next Few Days", @"Weeks", @"I'm Flexible",nil];
    
    [_pickerView setHidden:YES];
    
    _button_nextButton.layer.cornerRadius=_button_nextButton.frame.size.height/2;
    _button_nextButton.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _button_nextButton.layer.borderWidth=1.0f;
    _button_nextButton.clipsToBounds = YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}


#pragma mark  Tapped Actions

- (IBAction)TappedAction_BackButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)tappedAction_Done:(id)sender
{
    if (_ServiceTimeTextfield.text.length <= 0)
    {
        _ServiceTimeTextfield.text = [timeArray objectAtIndex:0];
    }
    
    [_pickerView setHidden:YES];
}

- (IBAction)TappedAction_ConfrimBooking:(id)sender
{
    if (_ServiceTimeTextfield.text.length <= 0)
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Please select the service time." : self];
    }
    else
    {
        ConfirmBookingController *controller = VCWithIdentifier(@"ConfirmBookingController");
        controller.serviceIDString = _TenderId;
        controller.startWorkString = _ServiceTimeTextfield.text;
        [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark  Textfield Delegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField.tag == 10)
    {
        [textField resignFirstResponder];
        [_pickerView setHidden:NO];
        _datapicker.delegate = self;
        _datapicker.dataSource = self;

    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldDidBeginEditing");
}


#pragma mark  Picker View Delegates

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
    return timeArray.count;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row  forComponent:(NSInteger)component {
    return  [timeArray objectAtIndex: row];
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    _ServiceTimeTextfield.text = [timeArray objectAtIndex:row];
}

- (UIView *)pickerView:(UIPickerView *)pickerView
            viewForRow:(NSInteger)row
          forComponent:(NSInteger)component
           reusingView:(UIView *)view
{
    UILabel *pickerLabel = (UILabel *)view;
    
    if (pickerLabel == nil)
    {
        CGRect frame = CGRectMake(5.0, 0.0, self.view.frame.size.width-10, 32);
        pickerLabel = [[UILabel alloc] initWithFrame:frame];
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:[UIFont boldSystemFontOfSize:15]];
    }
    [pickerLabel setText:[timeArray objectAtIndex: row]];
    return pickerLabel;
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
