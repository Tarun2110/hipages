//
//  TrendingViewController.m
//  Hi Pages
//
//  Created by Rakesh Kumar on 05/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "TenderingViewController.h"
#import "CollectionCell.h"
#import "TenderingDataObject.h"
#import "TenderingDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface TenderingViewController ()
@end
@implementation TenderingViewController


#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self ShowProgress];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    TenderingServicesArray = [NSMutableArray new];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.tabBarController.tabBar setHidden:NO];
    [self.APIService API_GetTenderingServices:self withSelector:@selector(OutputForTendring:) :self.view];
}

- (void) OutputForTendring:(NSDictionary *)responseDict
{
    [self HideProgress];
    int  successString = [[responseDict valueForKey:@"status"] intValue];
    if (successString == 1)
    {
        NSMutableArray *ServicesArray = [responseDict valueForKey:@"services"];
      
        for (int i = 0 ; i != ServicesArray.count ; i++)
        {
            TenderingDataObject * TenderObject = [TenderingDataObject new];
            TenderObject.TenderID =[[ServicesArray valueForKey:@"id"] objectAtIndex:i];
            TenderObject.TenderName = [[ServicesArray valueForKey:@"tendering"] objectAtIndex:i];
            TenderObject.TenderPagetitle = [[ServicesArray valueForKey:@"pagetitle"] objectAtIndex:i];
            TenderObject.TenderImage = [[ServicesArray valueForKey:@"image"] objectAtIndex:i];
            TenderObject.TenderDescription  = [[ServicesArray valueForKey:@"description"] objectAtIndex:i];
            
            [TenderingServicesArray addObject:TenderObject];
        }
        _TrendingCollectioView.delegate = self;
        _TrendingCollectioView.dataSource = self;
    }
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}

# pragma mark  CollectionView Delegates
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection: (NSInteger)section
{
    return TenderingServicesArray.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionCell *Cell = (CollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    Cell.contentView.frame = Cell.bounds;
    Cell.ServiceImage.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin  | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleTopMargin |UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    
    Cell.layer.masksToBounds = NO;
    Cell.layer.borderColor = [UIColor whiteColor].CGColor;
    Cell.layer.borderWidth = 2.0f;
    Cell.layer.contentsScale = [UIScreen mainScreen].scale;
    Cell.layer.shadowOpacity = 0.55f;
    Cell.layer.shadowRadius = 2.0f;
    Cell.layer.shadowOffset = CGSizeZero;
    Cell.layer.shadowPath = [UIBezierPath bezierPathWithRect:Cell.bounds].CGPath;
    Cell.layer.shouldRasterize = YES;
    
    TenderingDataObject * tenderObj = [TenderingServicesArray objectAtIndex:indexPath.row];
    Cell.ServiceName.text = tenderObj.TenderName ;
    [Cell.ServiceImage sd_setImageWithURL:[NSURL URLWithString:tenderObj.TenderImage]
                         placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    return Cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100, 100);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    // if(IS_IPHONE_5)
    {
        return UIEdgeInsetsMake(2, 2.5, 2, 2.5); // top, left, bottom, right
    }
    //    else if (IS_IPHONE_6)
    //    {
    //        return UIEdgeInsetsMake(10, 18, 10, 18); // top, left, bottom, right
    //    }
    //    else if (IS_IPHONE_6_PLUS)
    //    {
    //        return UIEdgeInsetsMake(25, 25, 25, 25); // top, left, bottom, right
    //    }
    //    return UIEdgeInsetsMake(25, 20, 25, 20); // top, left, bottom, right
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    TenderingDataObject * tenderObj = [TenderingServicesArray objectAtIndex:indexPath.row];
    TenderingDetailViewController * controller = VCWithIdentifier(@"TenderingDetailViewController");
    controller.tenderObj = tenderObj;
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
