//
//  ProfileViewController.h
//  Hi Pages
//
//  Created by Rakesh Kumar on 05/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"


@interface ProfileViewController : BaseViewController
{
    NSString *fname, *lname;
}
@property (strong, nonatomic) IBOutlet UIImageView *profilePic;
@property (strong, nonatomic) IBOutlet UIButton *orderhistoryButton;
@property (strong, nonatomic) IBOutlet UIView *BackGroundView;
@property (strong, nonatomic) IBOutlet UITextField *textfield_name;
@property (strong, nonatomic) IBOutlet UITextField *textfield_phone;
@property (strong, nonatomic) IBOutlet UITextField *textfield_email;

@end
