//
//  HomeViewController.h
//  Hi Pages
//
//  Created by Rakesh Kumar on 25/09/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CollectionCell.h"
#import "BaseViewController.h"
#import "CategoryObject.h"
#import <CoreLocation/CoreLocation.h>


@interface HomeViewController : BaseViewController<UICollectionViewDataSource,UICollectionViewDelegate,CLLocationManagerDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    IBOutlet UIView *pickerView;
    IBOutlet UIPickerView *DataPicker;
    IBOutlet UIButton *DoneButton;
    NSMutableArray *locationArray;
    IBOutlet UIButton *cityButton;
    
}

@property (strong, nonatomic) IBOutlet UICollectionView *PopularCollectionView;

@end
