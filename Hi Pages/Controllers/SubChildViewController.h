//
//  SubChildViewController.h
//  Hi Pages
//
//  Created by Rakesh Kumar on 15/11/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "CollectionCell.h"
#import "CategoryObject.h"


@interface SubChildViewController : BaseViewController<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic,strong) NSString* SubServiceId;
@property (nonatomic,strong) NSString* HeaderName;

@property (weak, nonatomic) IBOutlet UICollectionView *CollectioView;
@property (strong, nonatomic) IBOutlet UILabel *HeaderNameLabel;

@end
