//
//  SubChildViewController.m
//  Hi Pages
//
//  Created by Rakesh Kumar on 15/11/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "SubChildViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface SubChildViewController ()

@end

@implementation SubChildViewController

@synthesize SubServiceId;
NSMutableArray *SServicesArray;

#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    SServicesArray = [NSMutableArray new];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [self.tabBarController.tabBar setHidden:YES];
    _HeaderNameLabel.text = _HeaderName;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    NSString *cityId = [UserDefaults valueForKey:@"cityId"];
}


//- (void) OutputForSingle:(NSDictionary *)responseDict
//{
//    [self HideProgress];
//    int  successString = [[responseDict valueForKey:@"status"] intValue];
//    
//    NSMutableArray *DataArray = [responseDict valueForKey:@"ChildServices"];
//    for (int i = 0 ; i != DataArray.count ; i++)
//    {
//        CategoryObject * CatObj = [CategoryObject new];
//        CatObj.categoryID =[[DataArray valueForKey:@"id"] objectAtIndex:i];
//        CatObj.categoryName = [[DataArray valueForKey:@"service_name"] objectAtIndex:i];
//        CatObj.categoryImage = [[DataArray valueForKey:@"image"] objectAtIndex:i];
//      //  SubCatObj.ServiceNotes = [[DataArray valueForKey:@"notes"] objectAtIndex:i];
//        CatObj.categoryChild = [[DataArray valueForKey:@"child_subcat"] objectAtIndex:i];
//        CatObj.servicesArray = [[DataArray valueForKey:@"services"] objectAtIndex:i];
//        [SServicesArray addObject:CatObj];
//    }
//    _CollectioView.delegate = self;
//    _CollectioView.dataSource = self;
//}



-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}




# pragma mark  CollectionView Delegates
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection: (NSInteger)section
{
    return SServicesArray.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionCell *Cell = (CollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    Cell.contentView.frame = Cell.bounds;
    
    Cell.ServiceImage.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin  | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleTopMargin |UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    
    Cell.layer.masksToBounds = NO;
    Cell.layer.borderColor = [UIColor whiteColor].CGColor;
    Cell.layer.borderWidth = 2.0f;
    Cell.layer.contentsScale = [UIScreen mainScreen].scale;
    Cell.layer.shadowOpacity = 0.55f;
    Cell.layer.shadowRadius = 2.0f;
    Cell.layer.shadowOffset = CGSizeZero;
    Cell.layer.shadowPath = [UIBezierPath bezierPathWithRect:Cell.bounds].CGPath;
    Cell.layer.shouldRasterize = YES;
    
    CategoryObject * CatObj = [SServicesArray objectAtIndex:indexPath.row];
    
    Cell.ServiceName.text = CatObj.categoryName;
    
    [Cell.ServiceImage sd_setImageWithURL:[NSURL URLWithString:CatObj.categoryImage]
                         placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    return Cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100, 100);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    // if(IS_IPHONE_5)
    {
        return UIEdgeInsetsMake(2, 2.5, 2, 2.5); // top, left, bottom, right
    }
    //    else if (IS_IPHONE_6)
    //    {
    //        return UIEdgeInsetsMake(10, 18, 10, 18); // top, left, bottom, right
    //    }
    //    else if (IS_IPHONE_6_PLUS)
    //    {
    //        return UIEdgeInsetsMake(25, 25, 25, 25); // top, left, bottom, right
    //    }
    //    return UIEdgeInsetsMake(25, 20, 25, 20); // top, left, bottom, right
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
//    SubCategoryObject * CategoryObj = [SServicesArray objectAtIndex:indexPath.row];
//    
//    NSLog(@"%@",CategoryObj.ServiceChild_subcat);
//    
    
    //CategoryObj.ServiceName
    
    
    //    if ([CategoryObj.categoryChild  isEqual: @"true"])
    //    {
    //        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"bundle:nil];
    //        SubCategories * controller = [storyboard instantiateViewControllerWithIdentifier:@"SubCategories"];
    //        controller.ServiceId = CategoryObj.categoryID;
    //        [self.navigationController pushViewController:controller animated:YES];
    //    }
    //    else
    //    {
    //        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"bundle:nil];
    //        DetailViewController * controller = [storyboard instantiateViewControllerWithIdentifier:@"DetailViewController"];
    //        [self.navigationController pushViewController:controller animated:YES];
    //        
    //    }
}
- (IBAction)TappedAction_backButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
