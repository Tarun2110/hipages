//
//  MakeBookingViewController.h
//  Hi Pages
//
//  Created by Rakesh Kumar on 02/11/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"



@interface MakeBookingViewController : BaseViewController<UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIView *pickerView;
@property (strong, nonatomic) IBOutlet UIPickerView *datapicker;
@property (strong, nonatomic) IBOutlet UITextField *ServiceNameTextfield;
@property (strong, nonatomic) IBOutlet UITextField *ServiceTimeTextfield;
@property (strong, nonatomic) IBOutlet NSString *TenderName;
@property (strong, nonatomic) IBOutlet NSString *TenderId;

@property (strong, nonatomic) IBOutlet UIButton *button_nextButton;

@end
