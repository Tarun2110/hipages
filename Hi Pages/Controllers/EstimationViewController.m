//
//  EstimationViewController.m
//  Hi Pages
//
//  Created by Rakesh Kumar on 05/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "EstimationViewController.h"

@interface EstimationViewController ()

@end

@implementation EstimationViewController
{
    NSArray *DataArray;
    int i;
}
#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [_pickerView setAlpha:0.0f];
    DataArray = [NSArray new];
    _nextbutton.layer.cornerRadius=_nextbutton.frame.size.height/2;
    _nextbutton.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _nextbutton.layer.borderWidth=1.0f;
    _nextbutton.clipsToBounds = YES;
}

-(void) viewWillAppear:(BOOL)animated
{
    
    _label_where.text = @"Where do you want to build ?";
    _label_numberfloors.text =@"No. of Floors ?";
    _label_quality.text= @"Quality of Constructions ?";
}


- (IBAction)TappedAction_NextButton:(id)sender
{
    //[self.tabBarController.tabBar setHidden:NO];
   // [_pickerView setAlpha:0.0f];
    
    
    if ([_label_where.text isEqualToString:@"Where do you want to build ?"])
    {
        [self.GlobelFucntion alert:@"Alert" :@"Please select the city" :self];
    }
    else if ([_label_numberfloors.text isEqualToString:@"No. of Floors ?"])
    {
        [self.GlobelFucntion alert:@"Alert" :@"select number of floors." :self];
    }
    else if ([_label_quality.text isEqualToString:@"Quality of Constructions ?"])
    {
        [self.GlobelFucntion alert:@"Alert" :@"select the construction quality." :self];
    }
    else if (isStringEmpty(_textfield_area.text))
    {
        [self.GlobelFucntion alert:@"Alert" :@"Please enter the area in sq. ft." :self];
    }
    
    [self.tabBarController.tabBar setHidden:NO];

    
    
}

- (IBAction)TappedAction_SelectWhere:(id)sender
{
    [self.tabBarController.tabBar setHidden:YES];
    [_pickerView setAlpha:1.0f];
    i = 0 ;

    _datapicker.delegate = self;
    _datapicker.dataSource = self;
    DataArray = [NSArray arrayWithObjects: @"Chandigarh", @"Delhi",nil];


}

- (IBAction)TappedAction_selectFloors:(id)sender
{
    [self.tabBarController.tabBar setHidden:YES];
    [_pickerView setAlpha:1.0f];
    i = 1;
    
    _datapicker.delegate = self;
    _datapicker.dataSource = self;
    DataArray = [NSArray arrayWithObjects: @"Ground Floor", @"Ground Floor + 1", @"Ground Floor + 2", @"Ground Floor + 3",@"Ground Floor + 4",@"Ground Floor + 5",@"Ground Floor + 6",nil];

}

- (IBAction)TappedAction_SelectQuality:(id)sender
{
    [self.tabBarController.tabBar setHidden:YES];
    [_pickerView setAlpha:1.0f];
    i=2;
    
    _datapicker.delegate = self;
    _datapicker.dataSource = self;

    DataArray = [NSArray arrayWithObjects: @"Average", @"Medium", @"High",nil];

}

- (IBAction)TappedAction_DoneButton:(id)sender
{
    [_pickerView setAlpha:0.0f];

}


#pragma mark PickerView Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
    return DataArray.count;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row  forComponent:(NSInteger)component
{
    return  [DataArray objectAtIndex: row];
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (i == 0)
    {
        _label_where.text = [DataArray objectAtIndex:row];
    }
    else if (i == 1)
    {
        _label_numberfloors.text = [DataArray objectAtIndex:row];
    }
    else
    {
        _label_quality.text = [DataArray objectAtIndex:row];
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView
            viewForRow:(NSInteger)row
          forComponent:(NSInteger)component
           reusingView:(UIView *)view
{
    UILabel *pickerLabel = (UILabel *)view;
    
    if (pickerLabel == nil)
    {
        CGRect frame = CGRectMake(5.0, 0.0, self.view.frame.size.width-10, 32);
        pickerLabel = [[UILabel alloc] initWithFrame:frame];
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:[UIFont boldSystemFontOfSize:15]];
    }
    [pickerLabel setText:[DataArray objectAtIndex: row]];
    
      return pickerLabel;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
