//
//  EditProfileViewController.m
//  Hi Pages
//
//  Created by Rakesh Kumar on 06/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "EditProfileViewController.h"

@interface EditProfileViewController ()

@end

@implementation EditProfileViewController

#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tabBarController.tabBar setHidden:YES];
    
    _textfield_email.text = _email;
    _textfield_firstname.text =_firstname;
    _textfield_lastname.text =_lastname;
    _textfield_phoneNumber.text = _phoneNumber;
    
    _UserId = [UserDefaults valueForKey:@"UserId"];
    
    _SaveButton.layer.cornerRadius=_SaveButton.frame.size.height/2;
    _SaveButton.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _SaveButton.layer.borderWidth=1.0f;
    _SaveButton.clipsToBounds = YES;
    
    _changePasswordButton.layer.cornerRadius=_SaveButton.frame.size.height/2;
    _changePasswordButton.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _changePasswordButton.layer.borderWidth=1.0f;
    _changePasswordButton.clipsToBounds = YES;
    
    _backgroundView.layer.borderColor=[UIColor colorWithRed:220/255.0f green:220/255.0f blue:220/255.0f alpha:1.0].CGColor;
    _backgroundView.layer.borderWidth=1.0f;
    _backgroundView.clipsToBounds = YES;
    
    _EditProfilePic.layer.cornerRadius=_EditProfilePic.frame.size.height/2;
    _EditProfilePic.layer.borderColor=[UIColor colorWithRed:66/255.0f green:142/255.0f blue:241/255.0f alpha:1.0].CGColor;
    _EditProfilePic.layer.borderWidth=0.8f;
    _EditProfilePic.clipsToBounds = YES;
}



- (IBAction)TappedAction_Back:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}


- (IBAction)TappedAction_SaveButton:(id)sender
{
    if (isStringEmpty(_textfield_firstname.text))
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Please enter your first name." :self];
    }
    else if (isStringEmpty(_textfield_phoneNumber.text))
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Please enter your mobile phone number." :self];
    }
    else if (!validateEmailWithString(_textfield_email.text))
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Please enter valid email address." :self];
    }
    else
    {
        [self ShowProgress];
        [self.APIService API_EditUserDetails:self withSelector:@selector(outputForEditProfile:) :self.view :_UserId :_textfield_email.text :_textfield_firstname.text :_textfield_lastname.text :_textfield_phoneNumber.text];
    }
}

- (void) outputForEditProfile:(NSDictionary *)responseDict
{
    [self HideProgress];
    int  successString = [[responseDict valueForKey:@"status"] intValue];
    if (successString == 1)
    {
        [self.GlobelFucntion alertwithpop:@"Alert!" :@"Profile has been successfully updated." :self];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



@end
