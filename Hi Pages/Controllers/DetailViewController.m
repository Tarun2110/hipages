
//
//  DetailViewController.m
//  Hi Pages
//
//  Created by Rakesh Kumar on 25/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "DetailViewController.h"
#import "CustomTableViewCell.h"
@interface DetailViewController ()
{
    NSMutableArray * selectedServicesArr;
}
@end
NSMutableArray *ServiceDataArray;
NSMutableArray *mainArray;
NSMutableArray *PriceServiceArray;
NSIndexPath *indexPath;

@implementation DetailViewController

#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    _dataTableview.delegate = self;
    _dataTableview.dataSource = self;
    _dataTableview.estimatedRowHeight = 40;
    [_pickerView setAlpha:0.0f];
    _headerTitleLabel.text = _HeaderName;
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [self.tabBarController.tabBar setHidden:YES];
    
   
    ServiceDataArray = [NSMutableArray new];
    selectedServicesArr = [NSMutableArray new];
}

-(void) viewWillAppear:(BOOL)animated
{
    if (![[NSUserDefaults standardUserDefaults]boolForKey:@"subclass"])
    {
        [self ShowProgress];
        NSString *cityId = [UserDefaults valueForKey:@"cityId"];
        NSLog(@"--->>>%@",cityId);
        [self.APIService API_GetSingleServices:self withSelector:@selector(outputForSingleService:) :self.view :_DataObj.categoryID :cityId];
    }
    
    else
    {
        
        ServiceDataArray = _DataObj.servicesArray;
       // NSLog(@"%@",_DataObj.categoryName);
      //  NSLog(@"%@",_DataObj.categoryID);
        NSLog(@"%@",ServiceDataArray);

    }
    
    
}



- (void) outputForSingleService:(NSDictionary *)responseDict
{
    [self HideProgress];
    int  successString = [[responseDict valueForKey:@"kstatus"] intValue];
    if (successString == 1)
    {
        mainArray = [[[responseDict valueForKey:@"ChildServices"] valueForKey:@"services"] objectAtIndex:0];
        
        //mainArray = [responseDict valueForKey:@"ChildServices"] ;
                     
                     
        for (int i = 0 ; i != mainArray.count ; i++)
        {
            CategoryObject * SubCatObj = [CategoryObject new];
            SubCatObj.categoryName = [[mainArray valueForKey:@"servicename"] objectAtIndex:i];
            SubCatObj.unitStr = @"";
            SubCatObj.servicesArray = [[mainArray valueForKey:@"service"] objectAtIndex:i] ;
            [ServiceDataArray addObject:SubCatObj];
            
            CategoryObject * selectedObject = [CategoryObject new];
            selectedObject.selectdServiceName = [[mainArray valueForKey:@"servicename"] objectAtIndex:i];
            selectedObject.selectedServicePrice = @"";
            selectedObject.SelectedServiceUnit = @"";
            selectedObject.selected = false;
            
            [selectedServicesArr addObject:selectedObject];
        }
        
        NSLog(@"%@",ServiceDataArray);
        [_dataTableview reloadData];

        _dataTableview.frame = CGRectMake(0, 0, self.view.frame.size.width, _dataTableview.contentSize.height);
    }
}



- (IBAction)TappedAction_BackButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    BOOL check = false;
//    for (CategoryObject *object in selectedServicesArr) {
//        if (object.selected == true) {
//            check = true;
//        }
//    }
//    if (check == true) {
//        return ServiceDataArray.count+selectedServicesArr.count+1;
//    } else {
//        return ServiceDataArray.count+selectedServicesArr.count;
//    }
    
    return ServiceDataArray.count+selectedServicesArr.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if (ServiceDataArray.count > indexPath.row)
    {
        static NSString *VideoCellIdentifier = @"Custom";
        CustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:VideoCellIdentifier];
        
        if (cell == nil) {
            cell = [[[NSBundle mainBundle ] loadNibNamed:@"CustomTableViewCell" owner:self options:nil] objectAtIndex:0];
        }
       
        CategoryObject *catObj = [ServiceDataArray objectAtIndex:indexPath.row];
        cell.valuefield.tag = indexPath.row ;
        cell.droptapbutton.tag = cell.valuefield.tag;
        [cell.droptapbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
        if (![[NSUserDefaults standardUserDefaults]boolForKey:@"subclass"])
        {
            cell.titleLabel.text = catObj.categoryName;
            cell.valuefield.text = catObj.unitStr;
        }
       else
       {
           cell.titleLabel.text = [[ServiceDataArray valueForKey:@"servicename"]objectAtIndex:indexPath.row];
          // cell.valuefield.text = catObj.unitStr;
       }
        
        return cell;
    }
    else
    {
        static NSString *VideoCellIdentifier = @"CustomTableViewCell";
        CustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:VideoCellIdentifier];
        
        if (cell == nil)
        {
            cell = [[[NSBundle mainBundle ] loadNibNamed:@"CustomTableViewCell" owner:self options:nil] objectAtIndex:1];
        }
        
        CategoryObject *catObj = [selectedServicesArr objectAtIndex:indexPath.row-ServiceDataArray.count];
        
        cell.totalTitle.text = catObj.selectdServiceName;
        cell.pricelabel.text = catObj.selectedServicePrice;
        cell.unitlabel.text = catObj.SelectedServiceUnit;
        
        return cell;
    }
    
}

-(void)yourButtonClicked:(UIButton*)sender
{
    indexPath=[NSIndexPath indexPathForRow:sender.tag inSection:0]; // if section is 0
   
    if (![[NSUserDefaults standardUserDefaults]boolForKey:@"subclass"])
    {
        CategoryObject *catObj = [ServiceDataArray objectAtIndex:indexPath.row];
        PriceServiceArray = catObj.servicesArray ;
    }
    else
    {
        PriceServiceArray = [[ServiceDataArray valueForKey:@"service"] objectAtIndex:indexPath.row] ;

    }
    _dataPicker.delegate = self;
    _dataPicker.dataSource = self;
    [_pickerView setAlpha:1.0f];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (ServiceDataArray.count > indexPath.row)
    {
        return 55;
    }
    CategoryObject *cat1 = selectedServicesArr[indexPath.row-ServiceDataArray.count];
    if (cat1.selected==true)
    {
        return UITableViewAutomaticDimension;
    }
    else
    {
        return 0;
    }
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (IBAction)TappedAction_DoneButton:(id)sender
{
    [_pickerView setAlpha:0.0f];
    [_dataTableview reloadData];
    _dataTableview.frame = CGRectMake(0, 0, self.view.frame.size.width, _dataTableview.contentSize.height);
    
}


#pragma mark  Picker View Delegates

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
    return PriceServiceArray.count;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row  forComponent:(NSInteger)component {
    
    if (ServiceDataArray.count > indexPath.row)
    {
        return  [[PriceServiceArray objectAtIndex: row] valueForKey:@"unit"];
    }
    else
    {
        return [[ServiceDataArray objectAtIndex: row] valueForKey:@"unit"];
    }
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (ServiceDataArray.count > indexPath.row)
    {
        CustomTableViewCell *cell = (CustomTableViewCell*)[_dataTableview cellForRowAtIndexPath:indexPath];
        
        cell.valuefield.text= [NSString stringWithFormat:@"%@",[[PriceServiceArray objectAtIndex: row] valueForKey:@"unit"]];
        
        CategoryObject *cat = ServiceDataArray[indexPath.row];
        cat.unitStr = [NSString stringWithFormat:@"%@",[[PriceServiceArray objectAtIndex: row] valueForKey:@"unit"]];
        
        [ServiceDataArray replaceObjectAtIndex:indexPath.row withObject:cat];
        
        CategoryObject *cat1 = selectedServicesArr[indexPath.row];
        cat1.SelectedServiceUnit = [NSString stringWithFormat:@"%@",[[PriceServiceArray objectAtIndex: row] valueForKey:@"unit"]];
        cat1.selectedServicePrice = [NSString stringWithFormat:@"%@",[[PriceServiceArray objectAtIndex: row] valueForKey:@"price"]];
        cat1.selected = true;
        [selectedServicesArr replaceObjectAtIndex:indexPath.row withObject:cat1];

    }
    else
    {
        
    }
     // _ServiceTimeTextfield.text = [PriceServiceArray objectAtIndex:row];
}

- (UIView *)pickerView:(UIPickerView *)pickerView
            viewForRow:(NSInteger)row
          forComponent:(NSInteger)component
           reusingView:(UIView *)view
{
    UILabel *pickerLabel = (UILabel *)view;
    
    if (pickerLabel == nil)
    {
        CGRect frame = CGRectMake(5.0, 0.0, self.view.frame.size.width-10, 32);
        pickerLabel = [[UILabel alloc] initWithFrame:frame];
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:[UIFont boldSystemFontOfSize:15]];
    }
    if (![[NSUserDefaults standardUserDefaults]boolForKey:@"subclass"])

    {
        [pickerLabel setText:[[PriceServiceArray objectAtIndex: row] valueForKey:@"unit"]];

    }
    else
    {
        NSLog(@"%@",[[ServiceDataArray objectAtIndex:row ] valueForKey:@"unit"]);
        
    }
    return pickerLabel;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
