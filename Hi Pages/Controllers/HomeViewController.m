//  HomeViewController.m
//  Hi Pages
//  Created by Rakesh Kumar on 25/09/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.


#import "HomeViewController.h"

@interface HomeViewController ()
@end
@implementation HomeViewController

#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _PopularCollectionView.delegate = self;
    _PopularCollectionView.dataSource = self;
    [pickerView setAlpha:0.0f];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}




-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self ShowProgress];
    [self.APIService API_GetAllLocations:self withSelector:@selector(outputForLocations:) :self.view];

}
- (IBAction)TappedAction_SelectCity:(id)sender
{
    [pickerView setAlpha:1.0f];
    DataPicker.delegate = self;
    DataPicker.dataSource = self;
    [self.tabBarController.tabBar setHidden:YES];

}

- (void) outputForLocations:(NSDictionary *)responseDict
{
    [self HideProgress];
    int  successString = [[responseDict valueForKey:@"status"] intValue];
    if (successString == 1)
    {
        locationArray = [responseDict valueForKey:@"cities"];
        [UserDefaults setObject:locationArray forKey:@"locationArray"];
    }
}


-(void) viewDidAppear:(BOOL)animated
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9)
    {
        //locationManager.allowsBackgroundLocationUpdates = YES;
    }
    locationManager = [[CLLocationManager alloc] init];
    [locationManager requestWhenInUseAuthorization];
    locationManager.headingFilter = 1;
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if([CLLocationManager locationServicesEnabled])
    {
        NSLog(@"Location Services Enabled");
    }
    else
    {
       
    }
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            NSLog(@"Chaleya");
        }
            break;
        default:
        {
            [locationManager startUpdatingLocation];
        }
            break;
    }
}

#pragma mark CLLocationManager Delegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
 
    NSLog(@"Detected Location : %f, %f", currentLocation.coordinate.latitude, currentLocation.coordinate.longitude);
    
    
    NSString *chdLat = @"30.7333";
    NSString *chdLong = @"76.7794";

    NSString *DelhiLat = @"28.7041";
    NSString *DelhiLong = @"77.1025";
    
    CLLocation *Chdlocation = [[CLLocation alloc] initWithLatitude:[chdLat doubleValue] longitude:[chdLong doubleValue]];
    CLLocation *delhilocation = [[CLLocation alloc] initWithLatitude:[DelhiLat doubleValue] longitude:[DelhiLong doubleValue]];
    
    float ChdMeters = [Chdlocation distanceFromLocation:currentLocation];
    float DelhiMeters = [delhilocation distanceFromLocation:currentLocation];

    
    CLLocationDistance kilometers = ChdMeters / 1000.0;
    NSString *kms = [NSString stringWithFormat:@"%f",kilometers];
    NSArray *tempArray = [kms componentsSeparatedByString:@"."];
    kms = [tempArray objectAtIndex:0];
  
    int ChdValue = [kms intValue];
    
    CLLocationDistance DelhiKilo = DelhiMeters / 1000.0;
    NSString *Dkms = [NSString stringWithFormat:@"%f",DelhiKilo];
    NSArray *tempAr = [Dkms componentsSeparatedByString:@"."];
    Dkms = [tempAr objectAtIndex:0];
    
    int DelhiValue = [Dkms intValue];

    if (ChdValue > DelhiValue)
    {
        NSLog(@"Delhi");
        [UserDefaults setValue:@"2" forKey:@"cityId"];
    }
    else
    {
        NSLog(@"Chandigarh");
        [UserDefaults setValue:@"1" forKey:@"cityId"];
    }
    
}

# pragma mark  CollectionView Delegates
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection: (NSInteger)section
{
    return 6;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionCell *Cell = (CollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    Cell.contentView.frame = Cell.bounds;
    
    Cell.ServiceImage.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin  | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleTopMargin |UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    
    //  NSString *text = [[TotalArray valueForKey:@"page_title"] objectAtIndex:indexPath.row];
    
    Cell.ServiceName.text = @"Dummy Text";
    // Cell.ServiceName.textColor = [UIColor blackColor];
    
    Cell.ServiceImage.layer.borderColor=[UIColor lightGrayColor].CGColor;
    Cell.ServiceImage.layer.borderWidth=0.5f;
    Cell.ServiceImage.clipsToBounds = YES;
    //  NSString *ImageString = [[TotalArray valueForKey:@"image"]objectAtIndex:indexPath.row];
    
    //    if (![ImageString isKindOfClass:[NSNull class]])
    //    {
    //        NSURL *url = [NSURL URLWithString:ImageString];
    //
    //        Cell.ServiceImage.imageURL = url;
    //    }
    return Cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(80, 80);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    // if(IS_IPHONE_5)
    {
        return UIEdgeInsetsMake(2, 2.5, 2, 2.5); // top, left, bottom, right
    }
    //    else if (IS_IPHONE_6)
    //    {
    //        return UIEdgeInsetsMake(10, 18, 10, 18); // top, left, bottom, right
    //    }
    //    else if (IS_IPHONE_6_PLUS)
    //    {
    //        return UIEdgeInsetsMake(25, 25, 25, 25); // top, left, bottom, right
    //    }
    //    return UIEdgeInsetsMake(25, 20, 25, 20); // top, left, bottom, right
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
   
}

#pragma mark  Picker View Delegates

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
    return locationArray.count;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row  forComponent:(NSInteger)component {
    return  [[locationArray objectAtIndex: row] valueForKey:@"city_name"];
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    [cityButton setTitle:[[locationArray objectAtIndex:row] valueForKey:@"city_name"] forState:UIControlStateNormal];
}

- (UIView *)pickerView:(UIPickerView *)pickerView
            viewForRow:(NSInteger)row
          forComponent:(NSInteger)component
           reusingView:(UIView *)view
{
    UILabel *pickerLabel = (UILabel *)view;
    
    if (pickerLabel == nil)
    {
        CGRect frame = CGRectMake(5.0, 0.0, self.view.frame.size.width-10, 32);
        pickerLabel = [[UILabel alloc] initWithFrame:frame];
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:[UIFont boldSystemFontOfSize:15]];
    }
    [pickerLabel setText:[[locationArray objectAtIndex: row] valueForKey:@"city_name"]];
    return pickerLabel;
}


- (IBAction)TappedAction_DoneButton:(id)sender
{
    [pickerView setAlpha:0.0f];
    [self.tabBarController.tabBar setHidden:NO];
    if ([cityButton.titleLabel.text isEqualToString:@"Chandigarh"])
    {
        [UserDefaults setValue:@"1" forKey:@"cityId"];
    }
    else
    {
        [UserDefaults setValue:@"2" forKey:@"cityId"];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
