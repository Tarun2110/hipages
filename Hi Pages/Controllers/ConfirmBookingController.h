//
//  ConfirmBookingController.h
//  Hi Pages
//
//  Created by Rakesh Kumar on 06/11/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"


@interface ConfirmBookingController : BaseViewController<UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource>

@property (strong, nonatomic) IBOutlet UIButton *booking_button;
@property (strong, nonatomic) IBOutlet UIView *finshidView;
@property (strong, nonatomic) IBOutlet UIView *pickerView;
@property (strong, nonatomic) IBOutlet UIPickerView *dataPicker;

@property (strong, nonatomic) IBOutlet UITextField *textfield_FirstName;

@property (strong, nonatomic) IBOutlet UITextField *textfield_LastName;
@property (strong, nonatomic) IBOutlet UITextField *textfield_PhoneNumber;
@property (strong, nonatomic) IBOutlet UITextField *textfiled_Email;
@property (strong, nonatomic) IBOutlet UITextField *textfieldCity;
@property (strong, nonatomic) IBOutlet UITextField *textfield_Location;
@property (strong, nonatomic) IBOutlet UITextField *textfield_Address;
@property (strong, nonatomic) IBOutlet UITextField *textfield_description;
@property (strong, nonatomic) IBOutlet UIButton *homeButton;

@property (strong, nonatomic) IBOutlet NSString *startWorkString;
@property (strong, nonatomic) IBOutlet NSString *serviceIDString;
@property (strong, nonatomic) IBOutlet NSString *UserIDString;





@end
