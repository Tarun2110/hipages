//  EnterOTPViewController.m
//  Hi Pages
//  Created by Rakesh Kumar on 25/09/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.

#import "EnterOTPViewController.h"
#import "HomeViewController.h"

@interface EnterOTPViewController ()
@end
@implementation EnterOTPViewController

#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _textfieldOne.delegate = self;
    _textfieldTwo.delegate = self;
    _textfieldThree.delegate = self;
    _textfieldFour.delegate = self;
    _textfieldFive.delegate = self;

    _proceedButton.layer.cornerRadius=_proceedButton.frame.size.height/2;
    _proceedButton.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _proceedButton.layer.borderWidth=1.0f;
    _proceedButton.clipsToBounds = YES;
    
    _Label_verficationCode.text = [NSString stringWithFormat:@"Please enter the verification we've sent tp on +91 %@",_ContactNumber];
    
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.GlobelFucntion addBaseLineBelowTextField:@"" textfield:_textfieldOne];
    [self.GlobelFucntion addBaseLineBelowTextField:@"" textfield:_textfieldTwo];
    [self.GlobelFucntion addBaseLineBelowTextField:@"" textfield:_textfieldThree];
    [self.GlobelFucntion addBaseLineBelowTextField:@"" textfield:_textfieldFour];
    [self.GlobelFucntion addBaseLineBelowTextField:@"" textfield:_textfieldFive];
    
    
    [_textfieldOne becomeFirstResponder];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}

#pragma mark  Textfield Delegates

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ((textField.text.length < 1) && (string.length > 0))
    {
        NSInteger nextTag = textField.tag + 1;
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        if (! nextResponder){
            [textField resignFirstResponder];
        }
        textField.text = string;
        if (nextResponder)
            [nextResponder becomeFirstResponder];
        return NO;
        
    }else if ((textField.text.length >= 1) && (string.length > 0))
    {
        NSInteger nextTag = textField.tag + 1;
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        if (! nextResponder){
            [textField resignFirstResponder];
        }
        textField.text = string;
        if (nextResponder)
            [nextResponder becomeFirstResponder];
        return NO;
    }
    else if ((textField.text.length >= 1) && (string.length == 0)){
        NSInteger prevTag = textField.tag - 1;
        UIResponder* prevResponder = [textField.superview viewWithTag:prevTag];
        if (! prevResponder){
            [textField resignFirstResponder];
        }
        textField.text = string;
        if (prevResponder)
            [prevResponder becomeFirstResponder];
        return NO;
    }
    return YES;

}


#pragma mark  ProceedButton Action

- (IBAction)TappedAction_ProceedButton:(id)sender
{
    if (_textfieldOne.text.length <=0 || _textfieldTwo.text.length <=0 ||_textfieldThree.text.length <=0 || _textfieldFour.text.length <=0  )
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Please enter 5 digits OTP number." :self];
    }
    else
    {
        NSString *otpString = [NSString stringWithFormat:@"%@%@%@%@%@",_textfieldOne.text,_textfieldTwo.text,_textfieldThree.text,_textfieldFour.text,_textfieldFive.text];
        
        [self ShowProgress];
        
        [self.APIService API_VerifyOTP:self withSelector:@selector(OutputForOTP:) :self.view :_ContactNumber :otpString];
    }
}


- (void) OutputForOTP:(NSDictionary *)responseDict
{
    int  successString = [[responseDict valueForKey:@"status"] intValue];
    if (successString == 1)
    {
        NSString *UserId = [responseDict valueForKey:@"UserId"];
       
        [UserDefaults setValue:UserId forKey:@"UserId"];
        [UserDefaults setValue:_ContactNumber forKey:@"phone"];

        [self performSelector:@selector(navigateToHome) withObject:nil afterDelay:2.2f];
    }
    else
    {
        [self HideProgress];
        [self.GlobelFucntion alert:@"Alert!" :@"Something went wrong." :self];
    }
}

-(void) navigateToHome
{
    [self HideProgress];

    obj_delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    [obj_delegate addTabbar];
    [UserDefaults setBool:YES forKey:@"LoggedIn"];
    HomeViewController * controller  = VCWithIdentifier(@"HomeViewController");
    [self.navigationController pushViewController:controller animated:NO];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
