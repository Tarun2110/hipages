//
//  TenderingDetailViewController.h
//  Hi Pages
//
//  Created by Rakesh Kumar on 01/11/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TenderingDataObject.h"
#import "BaseViewController.h"

@interface TenderingDetailViewController : BaseViewController <UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *subCategoryArray;
}
@property (nonatomic,strong) NSString* HeaderName;
@property (nonatomic,strong) TenderingDataObject *tenderObj;
@property (strong, nonatomic) IBOutlet UILabel *TitleLabel;
@property (strong, nonatomic) IBOutlet UIImageView *ItemImage;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet UIButton *bookingButton;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
