//  ViewController.m
//  Hi Pages
//  Created by Rakesh Kumar on 19/09/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.

#import "RootViewController.h"
#import "LCBannerView.h"
#import "EnterOTPViewController.h"

@interface RootViewController ()<LCBannerViewDelegate>
@property (nonatomic, weak) LCBannerView *bannerView1;
@end
@implementation RootViewController

#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    _proceedButton.layer.cornerRadius=_proceedButton.frame.size.height/2;
    _proceedButton.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _proceedButton.layer.borderWidth=1.0f;
    _proceedButton.clipsToBounds = YES;
}

#pragma mark  BannerView Setup
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:true];
    [_BannerView addSubview:({
        
        LCBannerView *bannerView = [[LCBannerView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, _BannerView.frame.size.height)
                                                              delegate:nil
                                                             imageName:@"banner"
                                                                 count:3
                                                          timeInterval:3.0f
                                         currentPageIndicatorTintColor:[UIColor blueColor]
                                                pageIndicatorTintColor:[UIColor whiteColor]];
        bannerView.pageDistance = 20.0f;
        bannerView.didClickedImageIndexBlock = ^(LCBannerView *bannerView, NSInteger index) {
        };
        bannerView.didScrollToIndexBlock = ^(LCBannerView *bannerView, NSInteger index) {
        };
        self.bannerView1 = bannerView;
    })];
}

#pragma mark  ProceedButton Action
- (IBAction)Tappedaction_proceedButton:(id)sender
{
    if (_numberTextField.text.length <=0)
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Please enter your 10 digit phone number." :self];
    }
    else
    {
        [self ShowProgress];
        [self.APIService API_UserLogin:self withSelector:@selector(OutputforLogin:) :self.view :_numberTextField.text];
    }
}


- (void) OutputforLogin:(NSDictionary *)responseDict
{
    int  successString = [[responseDict valueForKey:@"status"] intValue];
    if (successString == 1)
    {
        [self HideProgress];

        EnterOTPViewController * controller = VCWithIdentifier(@"EnterOTPViewController");
        controller.ContactNumber =  _numberTextField.text;
        [self.navigationController pushViewController:controller animated:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
