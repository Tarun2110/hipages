//
//  ConfirmBookingController.m
//  Hi Pages
//
//  Created by Rakesh Kumar on 06/11/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "ConfirmBookingController.h"
#import "TenderingViewController.h"


@interface ConfirmBookingController ()
@end
@implementation ConfirmBookingController
{
    NSArray *MainArray,*locationArray;
    int i;
    int index;
    
    
    NSString *cityId, *locationId;
    
}

#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _booking_button.layer.cornerRadius=_booking_button.frame.size.height/2;
    _booking_button.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _booking_button.layer.borderWidth=1.0f;
    _booking_button.clipsToBounds = YES;
    
    _homeButton.layer.cornerRadius=_homeButton.frame.size.height/2;
    _homeButton.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _homeButton.layer.borderWidth=1.0f;
    _homeButton.clipsToBounds = YES;

    _finshidView.alpha = 0.0f;
    _pickerView.alpha = 0.0f;
    
     MainArray = [[UserDefaults objectForKey:@"locationArray"] mutableCopy] ;
    _textfield_PhoneNumber.text = [UserDefaults valueForKey:@"phone"];
    _textfiled_Email.text = [UserDefaults valueForKey:@"email"];
    _textfield_FirstName.text = [UserDefaults valueForKey:@"fname"];
    _textfield_LastName.text = [UserDefaults valueForKey: @"lname"];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    _textfieldCity.delegate = self;
    _textfield_Location.delegate = self;
    _textfield_Address.delegate = self;
    _textfield_LastName.delegate = self;
    _textfield_FirstName.delegate = self;
    _textfield_description.delegate = self;
    _textfield_PhoneNumber.delegate = self;
    _textfiled_Email.delegate = self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}

- (IBAction)TappedAction_BackButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)TappedAction_DoneButton:(id)sender
{
    
    [_dataPicker reloadAllComponents];

    if(i == 0)
    {
        if (_textfieldCity.text.length <=0)
        {
            _textfieldCity.text = [locationArray objectAtIndex:0];
          
            cityId = [[[[MainArray valueForKey:@"street"] objectAtIndex:0] valueForKey:@"city_id"] objectAtIndex:0];
           
            index = 0;
        }
    }
    else
    {
        if(isStringEmpty(_textfield_Location.text))
        {
            _textfield_Location.text = [locationArray objectAtIndex:0];
           
            locationId = [[[[MainArray valueForKey:@"street"] objectAtIndex:index] valueForKey:@"id"] objectAtIndex:0];
           
            index = 0;
            
        }
    }
    
    _pickerView.alpha = 0.0f;
}

- (IBAction)TappedAction_BookingButton:(id)sender
{
    if (isStringEmpty(_textfield_FirstName.text))
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Please enter your first name." :self];
    }
    else if (isStringEmpty(_textfield_LastName.text))
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Please enter your last name." :self];
    }
    else if (isStringEmpty(_textfield_PhoneNumber.text))
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Please enter your phone number." :self];
    }
    else if (!validateEmailWithString(_textfiled_Email.text))
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Please enter valid email address." :self];
    }
    else if (isStringEmpty(_textfieldCity.text))
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Please select your city." :self];
    }
    else if (isStringEmpty(_textfield_Location.text))
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Please select your/near by location." :self];
    }
    else if (isStringEmpty(_textfield_Address.text))
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Please fill up your address." :self];
    }
    else
    {
        [self ShowProgress];
      
        NSString * UserIdd = [UserDefaults valueForKey:@"UserId"];
      
        //NSLog(@"City id%@, Location ID%@",cityId,locationId);
        
        [self.APIService API_TenderBooking:self withSelector:@selector(outputForBooking:) :self.view :UserIdd :_textfield_FirstName.text :_textfield_LastName.text :cityId :locationId : _serviceIDString :_textfiled_Email.text :_textfield_PhoneNumber.text :_startWorkString :_textfield_Address.text :_textfield_description.text ];
    }
}


- (void) outputForBooking:(NSDictionary *)responseDict
{
    [self HideProgress];
    int  successString = [[responseDict valueForKey:@"status"] intValue];
    if (successString == 1)
    {
        _finshidView.alpha = 1.0f;
    }
}



-(void) hideKey
{
    [_textfieldCity resignFirstResponder];
    [_textfield_Location resignFirstResponder];
    [_textfield_Address resignFirstResponder];
    [_textfield_LastName resignFirstResponder];
    [_textfield_FirstName resignFirstResponder];
    [_textfield_description resignFirstResponder];
    [_textfield_PhoneNumber resignFirstResponder];
    [_textfiled_Email resignFirstResponder];
}

- (IBAction)TappedAction_Citybutton:(id)sender
{
    [self hideKey];
    _dataPicker.delegate = self;
    _dataPicker.dataSource = self;
    _pickerView.alpha = 1.0f;
    i = 0;
    locationArray = [MainArray valueForKey:@"city_name"];
    
    if (!isStringEmpty(_textfieldCity.text))
    {
        _textfield_Location.text = @"";
        _textfieldCity.text = @"";
        [_dataPicker selectRow:0 inComponent:0 animated:YES];

    }
}

- (IBAction)TappedAction_streetButton:(id)sender
{
    [self hideKey];
    
    if (isStringEmpty(_textfieldCity.text))
    {
        [self.GlobelFucntion alert:@"Alert!" :@"Please select your city." :self];
    }
    else
    {
        i = 1;
        _dataPicker.delegate = self;
        _dataPicker.dataSource = self;
        _pickerView.alpha = 1.0f;
        
        locationArray = [[[MainArray valueForKey:@"street"] objectAtIndex:index] valueForKey:@"street_name"];
    }
}




#pragma mark PickerView Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
    return locationArray.count;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row  forComponent:(NSInteger)component
{
    return  [locationArray objectAtIndex: row];
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (i == 0)
    {
        _textfieldCity.text = [locationArray objectAtIndex:row];
         index = (int) row;
        cityId = [[[[MainArray valueForKey:@"street"] objectAtIndex:row] valueForKey:@"city_id"] objectAtIndex:row];
        
        NSLog(@"%@",cityId);
    }
    else
    {
        _textfield_Location.text = [locationArray objectAtIndex:row];
        locationId = [[[[MainArray valueForKey:@"street"] objectAtIndex:index] valueForKey:@"id"] objectAtIndex:row];
        NSLog(@"%@",locationId);
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView
            viewForRow:(NSInteger)row
          forComponent:(NSInteger)component
           reusingView:(UIView *)view
{
    UILabel *pickerLabel = (UILabel *)view;
    
    if (pickerLabel == nil)
    {
        CGRect frame = CGRectMake(5.0, 0.0, self.view.frame.size.width-10, 32);
        pickerLabel = [[UILabel alloc] initWithFrame:frame];
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:[UIFont boldSystemFontOfSize:15]];
    }
    [pickerLabel setText:[locationArray objectAtIndex: row]];
   
    if (i == 0)
     {
         index = (int) row;
     }
    return pickerLabel;
}

- (IBAction)TappedAction_HomeButton:(id)sender
{
    NSArray *controllerArray = self.navigationController.viewControllers;
    for (id controller in controllerArray)
    {
        if ([controller isKindOfClass:[TenderingViewController class]])
        {
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



@end
