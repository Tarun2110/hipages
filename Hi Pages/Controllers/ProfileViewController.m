//
//  ProfileViewController.m
//  Hi Pages
//
//  Created by Rakesh Kumar on 05/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "ProfileViewController.h"
#import "EditProfileViewController.h"
#import "BookingViewController.h"


@interface ProfileViewController ()

@end

@implementation ProfileViewController

#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [self ShowProgress];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    _orderhistoryButton.layer.cornerRadius=_orderhistoryButton.frame.size.height/2;
    _orderhistoryButton.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _orderhistoryButton.layer.borderWidth=1.0f;
    _orderhistoryButton.clipsToBounds = YES;
    
    _profilePic.layer.cornerRadius=_profilePic.frame.size.height/2;
    _profilePic.layer.borderColor=[UIColor colorWithRed:66/255.0f green:142/255.0f blue:241/255.0f alpha:1.0].CGColor;
    _profilePic.layer.borderWidth=0.8f;
    _profilePic.clipsToBounds = YES;
    
    _BackGroundView.layer.borderColor = [UIColor colorWithRed:220/255.0f green:220/255.0f blue:220/255.0f alpha:1.0].CGColor;
    _BackGroundView.layer.borderWidth=1.0f;
    _BackGroundView.clipsToBounds = YES;
    
    [self.tabBarController.tabBar setHidden:NO];
    
    NSString *Userid = [UserDefaults valueForKey:@"UserId"];
    
    [self.APIService API_GetUserDetails:self withSelector:@selector(outputForTheProfile:) :self.view :Userid];
}


- (void) outputForTheProfile:(NSDictionary *)responseDict
{
    [self HideProgress];
    int  successString = [[responseDict valueForKey:@"status"] intValue];
    if (successString == 1)
    {
        fname = [responseDict valueForKey:@"first_name"];
        lname = [responseDict valueForKey:@"last_name"];
        
        _textfield_name.text =  [NSString stringWithFormat:@"%@ %@",[responseDict valueForKey:@"first_name"],[responseDict valueForKey:@"last_name"]];
        _textfield_phone.text =  [responseDict valueForKey:@"phone_no"];
        _textfield_email.text =  [responseDict valueForKey:@"email"];
        
        [UserDefaults setValue:_textfield_phone.text forKey:@"phone"];
        [UserDefaults setValue:_textfield_email.text forKey:@"email"];
        [UserDefaults setValue:fname forKey:@"fname"];
        [UserDefaults setValue:lname forKey:@"lname"];
    }
}



- (IBAction)TappedAction_editProfileButton:(id)sender
{
    EditProfileViewController * controller = VCWithIdentifier(@"EditProfileViewController");
    controller.firstname = fname;
    controller.lastname = lname;
    controller.email= _textfield_email.text;
    controller.phoneNumber= _textfield_phone.text;
    [self.navigationController pushViewController:controller animated:NO];
}

- (IBAction)TappedAction_OrderHistory:(id)sender
{
    BookingViewController * controller = VCWithIdentifier(@"BookingViewController");
    [self.navigationController pushViewController:controller animated:YES];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
