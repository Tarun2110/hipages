//
//  EstimationViewController.h
//  Hi Pages
//
//  Created by Rakesh Kumar on 05/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"


@interface EstimationViewController : BaseViewController<UIPickerViewDelegate,UIPickerViewDataSource>

@property (strong, nonatomic) IBOutlet UILabel *label_where;
@property (strong, nonatomic) IBOutlet UILabel *label_numberfloors;
@property (strong, nonatomic) IBOutlet UILabel *label_quality;
@property (strong, nonatomic) IBOutlet UITextField *textfield_area;
@property (strong, nonatomic) IBOutlet UIView *pickerView;
@property (strong, nonatomic) IBOutlet UIPickerView *datapicker;
@property (strong, nonatomic) IBOutlet UIButton *nextbutton;

@end
