//
//  SubCategories.m
//  Hi Pages
//
//  Created by Rakesh Kumar on 25/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "SubCategories.h"
#import "SubCategoryObject.h"
#import "SubChildViewController.h"
#import "DetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CategoryObject.h"


@interface SubCategories ()
@end
@implementation SubCategories
@synthesize ServiceId;
NSMutableArray *ServicesArray;

#pragma mark  View Setup

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    ServicesArray = [NSMutableArray new];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [self.tabBarController.tabBar setHidden:YES];
    _HeaderNameLabel.text = _HeaderName;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    NSString *cityId = [UserDefaults valueForKey:@"cityId"];
    [self.APIService API_GetSingleServices:self withSelector:@selector(OutputForSingle:) :self.view :ServiceId : cityId];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}


- (void) OutputForSingle:(NSDictionary *)responseDict
{
    [self HideProgress];
    int  successString = [[responseDict valueForKey:@"status"] intValue];
    
    NSMutableArray *DataArray = [responseDict valueForKey:@"ChildServices"];
    for (int i = 0 ; i != DataArray.count ; i++)
    {
        CategoryObject * SubCatObj = [CategoryObject new];
        SubCatObj.categoryID =[[DataArray valueForKey:@"id"] objectAtIndex:i];
        SubCatObj.categoryName = [[DataArray valueForKey:@"service_name"] objectAtIndex:i];
        SubCatObj.categoryImage = [[DataArray valueForKey:@"image"] objectAtIndex:i];
        SubCatObj.categoryChild = [[DataArray valueForKey:@"child_subcat"] objectAtIndex:i];
        SubCatObj.servicesArray = [[DataArray valueForKey:@"services"] objectAtIndex:i];
        [ServicesArray addObject:SubCatObj];
    }
    _CollectioView.delegate = self;
    _CollectioView.dataSource = self;
}


# pragma mark  CollectionView Delegates
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection: (NSInteger)section
{
    return ServicesArray.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionCell *Cell = (CollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    Cell.contentView.frame = Cell.bounds;
    
    Cell.ServiceImage.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin  | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleTopMargin |UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    
    Cell.layer.masksToBounds = NO;
    Cell.layer.borderColor = [UIColor whiteColor].CGColor;
    Cell.layer.borderWidth = 2.0f;
    Cell.layer.contentsScale = [UIScreen mainScreen].scale;
    Cell.layer.shadowOpacity = 0.55f;
    Cell.layer.shadowRadius = 2.0f;
    Cell.layer.shadowOffset = CGSizeZero;
    Cell.layer.shadowPath = [UIBezierPath bezierPathWithRect:Cell.bounds].CGPath;
    Cell.layer.shouldRasterize = YES;
    
    CategoryObject * SubCatObj = [ServicesArray objectAtIndex:indexPath.row];

    Cell.ServiceName.text = SubCatObj.categoryName;
    
    [Cell.ServiceImage sd_setImageWithURL:[NSURL URLWithString:SubCatObj.categoryImage]
                        placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    return Cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100, 100);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    // if(IS_IPHONE_5)
    {
        return UIEdgeInsetsMake(2, 2.5, 2, 2.5); // top, left, bottom, right
    }
    //    else if (IS_IPHONE_6)
    //    {
    //        return UIEdgeInsetsMake(10, 18, 10, 18); // top, left, bottom, right
    //    }
    //    else if (IS_IPHONE_6_PLUS)
    //    {
    //        return UIEdgeInsetsMake(25, 25, 25, 25); // top, left, bottom, right
    //    }
    //    return UIEdgeInsetsMake(25, 20, 25, 20); // top, left, bottom, right
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryObject * dataObj = [ServicesArray objectAtIndex:indexPath.row];
    if ([dataObj.categoryChild  isEqual: @"true"])
    {
        SubChildViewController *controller = VCWithIdentifier(@"SubChildViewController");
        controller.SubServiceId = dataObj.categoryID;
        [self.navigationController pushViewController:controller animated:YES];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults ]setBool:YES forKey:@"subclass"];

        DetailViewController *controller = VCWithIdentifier(@"DetailViewController");
        controller.DataObj = dataObj;
        [self.navigationController pushViewController:controller animated:YES];
    }
}


- (IBAction)TappedAction_BackButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



@end
