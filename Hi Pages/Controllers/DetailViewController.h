//
//  DetailViewController.h
//  Hi Pages
//
//  Created by Rakesh Kumar on 25/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "CategoryObject.h"


@interface DetailViewController : BaseViewController <UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *dataTableview;
@property (nonatomic,strong) NSString* HeaderName;
@property (strong, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (strong, nonatomic) CategoryObject *DataObj;

@property (strong, nonatomic) IBOutlet UIView *pickerView;
@property (strong, nonatomic) IBOutlet UIPickerView *dataPicker;

@end
