//
//  SubCategories.h
//  Hi Pages
//
//  Created by Rakesh Kumar on 25/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CollectionCell.h"
#import "BaseViewController.h"

@interface SubCategories : BaseViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic,strong) NSString* ServiceId;
@property (nonatomic,strong) NSString* HeaderName;

@property (weak, nonatomic) IBOutlet UICollectionView *CollectioView;
@property (strong, nonatomic) IBOutlet UILabel *HeaderNameLabel;

@end
