//
//  AppDelegate.m
//  Hi Pages
//
//  Created by Rakesh Kumar on 19/09/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeViewController.h"
#import "RootViewController.h"


@interface AppDelegate ()

@end

@implementation AppDelegate
UINavigationController *navController;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"LoggedIn"])
    {
        HomeViewController *HomeVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"]; //or the homeController
        navController = [[UINavigationController alloc]initWithRootViewController:HomeVC];
        self.window.rootViewController = navController;
        [self.window addSubview:navController.view];
        [self addTabbar];
    }
    else
    {
        RootViewController *RootController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"RootViewController"];
        navController = [[UINavigationController alloc]initWithRootViewController:RootController];
        self.window.rootViewController = navController;
    }
    [self.window makeKeyAndVisible];

    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (void)addTabbar
{
    self.tabBarController = [[UITabBarController alloc] init];
    
    self.tabBarController.delegate = self;
    
    UIViewController *viewController1 = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"]; //or the homeController
    
    UIViewController *viewController2 = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ServicesViewController"]; //or the homeController
    
    UIViewController *viewController3 = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"TenderingViewController"]; //or the homeController
    
    UIViewController *viewController4 = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"EstimationViewController"]; //or the homeController
    
    UIViewController *viewController5 = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProfileViewController"]; //or the homeController
    
    
    
    UINavigationController *navigationController1 = [[UINavigationController alloc] initWithRootViewController:viewController1];
    
    UINavigationController *navigationController2 = [[UINavigationController alloc] initWithRootViewController:viewController2];
    
    UINavigationController *navigationController3 = [[UINavigationController alloc] initWithRootViewController:viewController3];
    
    UINavigationController *navigationController4 = [[UINavigationController alloc] initWithRootViewController:viewController4];
    
    UINavigationController *navigationController5 = [[UINavigationController alloc] initWithRootViewController:viewController5];
    
    
    self.tabBarController.viewControllers = [NSArray arrayWithObjects:navigationController1,navigationController2, navigationController3,navigationController4,navigationController5,nil];
    
    
    _tabBar = _tabBarController.tabBar;
    UIImageView *tabBackground = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.window.bounds.size.width, 50)];
    
    tabBackground.backgroundColor = [UIColor colorWithRed:66/255.0f green:142/255.0f blue:241/255.0f alpha:1.0];
    tabBackground.contentMode = UIViewContentModeScaleAspectFill;
    [self.tabBar insertSubview:tabBackground atIndex:0];
    
    UITabBarItem *tabBarItem1 = [_tabBar.items objectAtIndex:0];
    UITabBarItem *tabBarItem2 = [_tabBar.items objectAtIndex:1];
    UITabBarItem *tabBarItem3 = [_tabBar.items objectAtIndex:2];
    UITabBarItem *tabBarItem4 = [_tabBar.items objectAtIndex:3];
    UITabBarItem *tabBarItem5 = [_tabBar.items objectAtIndex:4];
    
     UIImage *active_image   = [UIImage imageNamed:@"tap_bar_icn_home"];
     UIImage *inactive_image = [UIImage imageNamed:@"tap_bar_icn_home_normal"];
    tabBarItem1.imageInsets = UIEdgeInsetsMake(0, -10, -6, -10);

    [tabBarItem1 setImage:[inactive_image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem1 setSelectedImage:[active_image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    tabBarItem1.title = @"Home";
    tabBarItem1.tag = 1;
    
    
    [tabBarItem2 setImage:[[UIImage imageNamed:@"tap_bar_icn_service_normal"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem2 setSelectedImage:[[UIImage imageNamed:@"tap_bar_icn_service"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    tabBarItem2.imageInsets = UIEdgeInsetsMake(0, -10, -6, -10);

    tabBarItem2.title = @"Services";
    tabBarItem2.tag = 2;
    
    
    
    [tabBarItem3 setImage:[[UIImage imageNamed:@"tap_bar_icn_tendering_normal"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem3 setSelectedImage:[[UIImage imageNamed:@"tap_bar_icn_tendering"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    tabBarItem3.title = @"Trending";
    
    tabBarItem3.imageInsets = UIEdgeInsetsMake(0, -10, -6, -10);
    tabBarItem3.tag = 3;
    
    
    [tabBarItem4 setImage:[[UIImage imageNamed:@"tap_bar_icn_estimation_normal"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem4 setSelectedImage:[[UIImage imageNamed:@"tap_bar_icn_estimation"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    tabBarItem4.imageInsets = UIEdgeInsetsMake(0, -10, -6, -10);

    tabBarItem4.title = @"Estimation";
    tabBarItem4.tag = 4;
    
    
    
    [tabBarItem5 setImage:[[UIImage imageNamed:@"tap_bar_icn_profile_normal"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem5 setSelectedImage:[[UIImage imageNamed:@"tap_bar_icn_profile"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    tabBarItem5.imageInsets = UIEdgeInsetsMake(0, -10, -6, -10);
    tabBarItem5.title = @"Profile";
    tabBarItem5.tag = 5;
    
    [self.tabBar setTintColor:[UIColor whiteColor]];
    [self.tabBar setBarTintColor:[UIColor whiteColor]];
    self.window.rootViewController = self.tabBarController;
    
}

@end
