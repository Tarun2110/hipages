//
//  EnterOTPViewController.h
//  Hi Pages
//
//  Created by Rakesh Kumar on 25/09/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "AppDelegate.h"

@interface EnterOTPViewController : BaseViewController <UITextFieldDelegate>
{
    AppDelegate *obj_delegate;

}
@property (strong, nonatomic) IBOutlet UITextField *textfieldOne;
@property (strong, nonatomic) IBOutlet UITextField *textfieldTwo;
@property (strong, nonatomic) IBOutlet UITextField *textfieldThree;
@property (strong, nonatomic) IBOutlet UITextField *textfieldFour;
@property (strong, nonatomic) IBOutlet UITextField *textfieldFive;

@property (strong, nonatomic) IBOutlet UIButton *proceedButton;
@property (strong, nonatomic) IBOutlet NSString *ContactNumber;
@property (strong, nonatomic) IBOutlet UILabel *Label_verficationCode;

@end
