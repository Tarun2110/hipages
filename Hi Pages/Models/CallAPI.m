#import "CallAPI.h"
#import "MPNotificationView.h"
@implementation CallAPI

@synthesize reachability;
@synthesize internetWorking;
@synthesize callBackSelector;
@synthesize callBackTarget;


- (id)init {
    if (self = [super init])
    {
        _GlobelFucntion = [GlobelFunctions new];
    }
    return self;
}

- (void) initAuthorizationFor:(NSString *)methodType
{
}

- (NSString *)setAccessToken
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
}




#pragma mark ********************
#pragma mark  Login API Functions
#pragma mark ********************

- (void)API_UserLogin:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)phoneNumber
{
    [self checkInternetConnection];
    if (internetWorking == 1)
    {
        NSDictionary *headers = @{
                                  @"content-type": @"application/json",
                                  };
        NSDictionary *parameters = @
        {
            @"phone_no": phoneNumber
        };
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@login",BaseUrl]]];
        request.timeoutInterval = 25.0;
        
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
    
}


#pragma mark ********************
#pragma mark  Verfify Login API
#pragma mark ********************

- (void)API_VerifyOTP:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)phone_no  :(NSString *)otp

{
    [self checkInternetConnection];
    if (internetWorking == 1)
    {
        NSDictionary *headers = @{
                                  @"content-type": @"application/json",
                                  };
        NSDictionary *parameters = @
        {
            @"phone_no": phone_no,
            @"otp": otp
        };
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@verifyotp",BaseUrl]]];
        request.timeoutInterval = 25.0;
        
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
    
}



#pragma mark ********************
#pragma mark  Get User Details API
#pragma mark ********************

- (void)API_GetUserDetails:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)UserId
{
    [self checkInternetConnection];
    if (internetWorking == 1)
    {
        NSDictionary *headers = @{
                                  @"content-type": @"application/json",
                                  };
        NSDictionary *parameters = @
        {
            @"user_id": UserId
        };
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@profile",BaseUrl]]];
        request.timeoutInterval = 25.0;
        
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}

- (void)API_EditUserDetails:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)user_id :(NSString *)email :(NSString *)first_name :(NSString *)last_name :(NSString *)phone_no
{
    [self checkInternetConnection];
    if (internetWorking == 1)
    {
        NSDictionary *headers = @{
                                  @"content-type": @"application/json",
                                  };
        NSDictionary *parameters = @
        {
            @"user_id": user_id,
            @"email": email,
            @"first_name": first_name,
            @"last_name": last_name,
            @"phone_no": phone_no
        };
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@editprofile",BaseUrl]]];
        request.timeoutInterval = 25.0;
        
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }

}

- (void)API_GetAllServices:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSDictionary *headers = @{
                                  @"content-type": @"application/json",
                                  };
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@allservices",BaseUrl]]];
        request.timeoutInterval = 25.0;
        
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}

- (void)API_GetSingleServices:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)ServiceId : (NSString *)CityId
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSDictionary *headers = @{
                                  @"content-type": @"application/json",
                                  };
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@singleservice?service_id=%@&city_id=%@",BaseUrl,ServiceId,CityId]]];
        request.timeoutInterval = 25.0;
        
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}


- (void)API_GetTenderingServices:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView
{
    [self checkInternetConnection];
    if (internetWorking == 1)
    {
        NSDictionary *headers = @{
                                  @"content-type": @"application/json",
                                  };
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@tendering",BaseUrl]]];
        request.timeoutInterval = 25.0;
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}


- (void)API_TenderBooking:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)user_id :(NSString *)first_name :(NSString *)last_name :(NSString *)city :(NSString *)location :(NSString *)service_id :(NSString *)email :(NSString *)phone_no :(NSString *)start_work :(NSString *)address :(NSString *)description
{
    [self checkInternetConnection];
    
    if (internetWorking == 1)
    {
        NSDictionary *headers = @{
                                @"content-type": @"application/json",
                                };

        NSDictionary *parameters = @{@"user_id": user_id,
                                     @"first_name": first_name,
                                     @"last_name": last_name,
                                     @"city": city,
                                     @"location": location,
                                     @"service_id": service_id,
                                     @"email": email,
                                     @"phone_no": phone_no,
                                     @"start_work": start_work,
                                     @"address": address,
                                     @"description": description
                                     };
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@tenderingbooking",BaseUrl]]];
        request.timeoutInterval = 25.0;
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
    
}

- (void)API_GetAllLocations:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView
{
    [self checkInternetConnection];
    if (internetWorking == 1)
    {
        NSDictionary *headers =   @{
                                  @"content-type": @"application/json",
                                  };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@city",BaseUrl]]];
        request.timeoutInterval = 25.0;
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}


- (void)API_BookingHistory:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)user_id
{
    [self checkInternetConnection];
    if (internetWorking == 1)
    {
        NSDictionary *headers = @{
                                  @"content-type": @"application/json",
                                  };
        NSDictionary *parameters = @{
                                     @"user_id": user_id
                                     };
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@history",BaseUrl]]];
        request.timeoutInterval = 25.0;
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}

//#pragma mark **********************
//#pragma mark - Output Section
//#pragma mark **********************

- (void)handleOutputForOther:(NSMutableURLRequest *)urlRequest withTarget:(id)tempTarget withSelector:(SEL)tempSelector
{
    NSURLSession *session = [NSURLSession sharedSession];
    NSDictionary *dict;
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if (error)
                                          {
                                              NSLog(@"%@", error);
                                          }
                                          else
                                          {
                                              NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                              
                                              NSMutableDictionary *jsonResponse = [[NSMutableDictionary alloc] init];
                                              
                                              jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments                                                                                                            error:&error];
                                              
                                              [self showOutPutHereWithTarget:tempTarget withSelector:tempSelector withDictionary:jsonResponse];
                                              
                                          }
                                      }];
    [dataTask resume];
}



- (void)showOutPutHereWithTarget:(id)tempTarget withSelector:(SEL)stateSelector  withDictionary:(NSDictionary *)dict
{
    [tempTarget performSelectorOnMainThread:stateSelector withObject:dict waitUntilDone:YES];
}

#pragma mark **********************
#pragma mark - Reachability Methods
#pragma mark **********************

- (void)checkInternetConnection
{
    reachability = [Reachability reachabilityForInternetConnection];
    [self performSelector:@selector(updateInterfaceWithReachability:)withObject:reachability];
}

- (void)updateInterfaceWithReachability:(Reachability *)curReach
{
    if(curReach == reachability)
    {
        NetworkStatus netStatus = [curReach currentReachabilityStatus];
        switch (netStatus)
        {
            case NotReachable:
            {
                internetWorking = 0;
                [MPNotificationView notifyWithText:@"No Network Connection"
                                            detail:@"Connect to a Wi-Fi network or a cellular data."
                                             image:nil
                                          duration:2.0
                                              type:@"Custom"
                                     andTouchBlock:^(MPNotificationView *notificationView)
                 {
                     NSLog( @"Received touch for notification with text: %@", notificationView.textLabel.text );
                 }];
                //  HideProgressHUD;
                break;
            }
            case ReachableViaWiFi:
            {
                internetWorking = 1;
                break;
            }
            case ReachableViaWWAN:
            {
                internetWorking = 1;
                break;
            }
        }
    }
}


-(void)noNetworkAlert
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowServerError" object:nil];
}

@end
