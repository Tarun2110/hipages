#import <Foundation/Foundation.h>
#import "GlobelFunctions.h"
#import "Reachability.h"

@class BaseViewController;


@interface CallAPI : NSObject<NSXMLParserDelegate>
{
    __weak NSString *deviceName;
    
    NSURLConnection *theConnection;
    NSMutableData   *mutResponseData;
    int             intResponseCode;
    Reachability    *reachability;
    NSString *str_currentElement;
    SEL callBackSelector;
    id __weak callBackTarget;
}


@property (strong ,nonatomic) BaseViewController *objBaseVC;
@property (strong,nonatomic) GlobelFunctions *GlobelFucntion;
@property (nonatomic, strong) Reachability *reachability;
@property (nonatomic, readwrite) NSInteger internetWorking;
@property (nonatomic) SEL callBackSelector;
@property (nonatomic, weak) id callBackTarget;

- (void)checkInternetConnection;

//API FOR USER AUTHENTICATION//




- (void)API_UserLogin:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)phoneNumber ;

- (void)API_VerifyOTP:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)phone_no  :(NSString *)otp;

- (void)API_GetUserDetails:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)UserId ;

- (void)API_EditUserDetails:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)user_id :(NSString *)email :(NSString *)first_name :(NSString *)last_name :(NSString *)phone_no ;

- (void)API_GetAllLocations:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView;

- (void)API_GetAllServices:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView;

- (void)API_GetSingleServices:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)ServiceId :(NSString *)CityId;

- (void)API_GetTenderingServices:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView;

- (void)API_TenderBooking:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)user_id :(NSString *)first_name :(NSString *)last_name :(NSString *)city :(NSString *)location :(NSString *)service_id :(NSString *)email :(NSString *)phone_no :(NSString *)start_work :(NSString *)address :(NSString *)description;

- (void)API_BookingHistory:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)user_id;

@end
