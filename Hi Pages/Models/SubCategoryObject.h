//
//  SubCategoryObject.h
//  Hi Pages
//
//  Created by Rakesh Kumar on 27/10/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubCategoryObject : NSObject
@property (nonatomic, strong) NSString * ServiceName, *ServiceId, *ServiceImage ,*ServiceNotes, *ServiceChild_subcat;
@property (nonatomic, strong) NSMutableArray * SubServiceArray;


@end
